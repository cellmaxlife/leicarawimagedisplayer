
#pragma once
#include <afxwin.h>

class ImageDisplayerApp : public CWinApp
{
public:
	// override InitInstance to do our initialisation
	virtual BOOL InitInstance();
};

class ImageDisplayerWnd : public CFrameWnd
{
public:
	// constructor to create the window
	ImageDisplayerWnd(CString filename);
private:
	int ViewWidth_;
	int ViewHeight_;
	int HScrollPos_;
	int VScrollPos_;
	int HPageSize_;
	int VPageSize_;
	CString m_Filename;
	int m_RedContrast;
	int m_GreenContrast;
	int m_BlueContrast;
	int m_RedCutoff;
	int m_GreenCutoff;
	int m_BlueCutoff;
	int m_RedCPI;
	int m_GreenCPI;
	int m_BlueCPI;
	int m_NumFrames;
	int m_NumRows;
	int m_NumColumns;

protected:
	BYTE GetContrastEnhancedByte(unsigned short value, int contrast, int cutoff);
	// declare our handlers
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT);
	afx_msg void OnSize(UINT, int, int);
	afx_msg void OnHScroll(UINT, UINT, CScrollBar*);
	afx_msg void OnVScroll(UINT, UINT, CScrollBar*);
	afx_msg BOOL OnMouseWheel(UINT, short, CPoint);
	afx_msg void OnClose();
	// this declares the message map and should be
	// the last class member
	DECLARE_MESSAGE_MAP()
	bool CopyToRGBImage(CString filename);
	bool LoadTIFFImages(CString filename);
	int m_ImageWidth1, m_ImageHeight1;
	int m_ImageWidth2, m_ImageHeight2;
	CDC m_dcMem;
	CImage m_Image1;
	CImage m_Image2;
	bool m_DisplayMap1;
	double m_ZoomFactor;
	BOOL PreTranslateMessage(MSG* pMsg);
	bool LoadScanFile(CString filename);
	bool SaveOverlayImageToFile(CString pathname, CString safeFilename);
	BOOL SaveBMPImage(CString filename, CImage *image);
};

