//*******************************************************************************
// MFC Scrolling demo
// Code is provided as is. You may do whatever you like with it.
//*******************************************************************************
#include "stdafx.h"
#include "ImageDisplayer.h"
#include "TIFFFrame.h"

#define MIN_ZOOM_FACTOR1	1.0
#define MAX_ZOOM_FACTOR1	16.0
#define MIN_ZOOM_FACTOR2    17.0
#define MAX_ZOOM_FACTOR2	27.0

#define MAX_SCREEN_WIDTH	2400
#define MAX_SCREEN_HEIGHT	1600

#define TIFF_FRAME_WIDTH	1344
#define TIFF_FRAME_HEIGHT	1024
#define NON_OVERLAP_WIDTH	1214
#define NON_OVERLAP_HEIGHT	927

// global constant
const int LINESIZE = 8;
// our application object. must be global
ImageDisplayerApp theApp;
// The usual initialisation
BOOL ImageDisplayerApp::InitInstance()
{
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Leica SCAN files (*.SCAN)|*.SCAN"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_pMainWnd = new ImageDisplayerWnd(filename);
		m_pMainWnd->ShowWindow(m_nCmdShow);
		m_pMainWnd->UpdateWindow();
		return TRUE;
	}
	else
	{
		AfxMessageBox(_T("Failed to specify a Red Channel TIFF file"));
		return FALSE;
	}
}
// The message map
BEGIN_MESSAGE_MAP(ImageDisplayerWnd, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_MOUSEWHEEL()
	ON_WM_CLOSE()
END_MESSAGE_MAP()
// constructor to create the window
ImageDisplayerWnd::ImageDisplayerWnd(CString filename)
{
	m_Filename = filename;
	Create(NULL, _T("ImageDisplayer(V1)"), WS_OVERLAPPEDWINDOW | WS_HSCROLL | WS_VSCROLL);
}

void ImageDisplayerWnd::OnClose()
{
	CFrameWnd::OnClose();
}

// this gets called on a WM_CREATE
int ImageDisplayerWnd::OnCreate(LPCREATESTRUCT createstruct)
{
	// always call the base class OnCreate to give the FrameWnd
	// chance to initialise
	if (CFrameWnd::OnCreate(createstruct) == -1) return -1;
	m_RedCutoff = 20;
	m_RedContrast = 760;
	m_GreenCutoff = 20;
	m_GreenContrast = 900;
	m_BlueCutoff = 0;
	m_BlueContrast = 1000;
	m_RedCPI = 289;
	m_GreenCPI = 240;
	m_BlueCPI = 243;
	if (!LoadTIFFImages(m_Filename))
	{
		AfxMessageBox(_T("Failed to load TIFF Frame Image Files"));
		return TRUE;
	}
	else
	{
		bool ret = false;
		CString message;
		CString resultPathname = _T(".\\OverlayImages");
		if (!PathIsDirectory(resultPathname))
		{
			if (!CreateDirectory(resultPathname, NULL))
			{
				resultPathname = "";
			}
		}
		if (resultPathname.GetLength() > 0)
		{
			WCHAR *char1 = _T("\\");
			int index = m_Filename.ReverseFind(*char1);
			if (index > -1)
			{
				CString safeFilename = m_Filename.Mid(index + 1);
				index = safeFilename.Find(_T("."));
				if (index > -1)
				{
					CString filename1 = safeFilename.Mid(0, index);
					safeFilename.Format(_T("%s.bmp"), filename1);
					ret = SaveOverlayImageToFile(resultPathname, safeFilename);
					if (ret)
					{
						message.Format(_T("Saved Overlay Image %s in OverlayImages subfolder"), safeFilename);
					}
					else
					{
						message.Format(_T("Failed to save %s file"), safeFilename);
					}
				}
				else
				{
					message.Format(_T("Failed to find scan file extension in %s"), safeFilename);
				}
			}
			else
			{
				message.Format(_T("Failed to find safeFilename in %s"), m_Filename);
			}
		}
		else
		{
			message = _T("Failed to create OverlayImages subfolder");
		}
		AfxMessageBox(message);
		if (!ret)
			return TRUE;
	}
	// get a client area DC for this window
	CClientDC dc(this);
	m_dcMem.CreateCompatibleDC(&dc);
	CImage image;
	image.Create(MAX_SCREEN_WIDTH, -MAX_SCREEN_HEIGHT, 24);
	BYTE *pCursor = (BYTE *)image.GetBits();
	memset(pCursor, 0, sizeof(BYTE) * image.GetPitch() * MAX_SCREEN_HEIGHT);
	CBitmap bitmap;
	bitmap.Attach(image.Detach());
	CBitmap *pOldMap = m_dcMem.SelectObject(&bitmap);
	if (pOldMap != NULL)
		pOldMap->DeleteObject();
	m_ZoomFactor = MIN_ZOOM_FACTOR1;
	m_DisplayMap1 = true;
	ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
	ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
	HScrollPos_ = 0;
	VScrollPos_ = 0;
	// return 0 if all went well
	return 0;
}
// this gets called on WM_SIZE
void ImageDisplayerWnd::OnSize(UINT type, int x, int y)
{
	// call base class OnSize
	CFrameWnd::OnSize(type, x, y);
	HPageSize_ = x;
	VPageSize_ = y;
	SCROLLINFO si;
	si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewWidth_;
	si.nPos = HScrollPos_;
	si.nPage = HPageSize_;
	SetScrollInfo(SB_HORZ, &si, TRUE);
	si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewHeight_;
	si.nPos = VScrollPos_;
	si.nPage = VPageSize_;
	SetScrollInfo(SB_VERT, &si, TRUE);
}
// This gets called on a WM_PAINT
void ImageDisplayerWnd::OnPaint()
{
	// get a painting DC
	if ((m_Image1 != NULL) && (m_Image2 != NULL))
	{
		CPaintDC dc(this);
		CRect rect;
		GetClientRect(&rect);
		if (m_DisplayMap1)
		{
			dc.SetMapMode(MM_ISOTROPIC);
			dc.SetWindowExt(100, 100);
			dc.SetViewportExt((int)(100 * m_ZoomFactor), (int)(100 * m_ZoomFactor));
			int startX = (int)(HScrollPos_ / m_ZoomFactor);
			int startY = (int)(VScrollPos_ / m_ZoomFactor);
			int width = rect.Width();
			if ((m_ImageWidth1 - startX) < width)
				width = m_ImageWidth1 - startX;
			else if (width > MAX_SCREEN_WIDTH)
				width = MAX_SCREEN_WIDTH;
			int height = rect.Height();
			if ((m_ImageHeight1 - startY) < height)
				height = m_ImageHeight1 - startY;
			else if (height > MAX_SCREEN_HEIGHT)
				height = MAX_SCREEN_HEIGHT;
			m_Image1.BitBlt(m_dcMem, 0, 0, width, height, startX, startY, SRCCOPY);
			dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &m_dcMem, 0, 0, SRCCOPY);
		}
		else
		{
			dc.SetMapMode(MM_ISOTROPIC);
			dc.SetWindowExt(100, 100);
			dc.SetViewportExt((int)(100 * (m_ZoomFactor - MAX_ZOOM_FACTOR1)), (int)(100 * (m_ZoomFactor - MAX_ZOOM_FACTOR1)));
			int startX = (int)(HScrollPos_ / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
			int startY = (int)(VScrollPos_ / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
			int width = rect.Width();
			if ((m_ImageWidth2 - startX) < width)
				width = m_ImageWidth2 - startX;
			else if (width > MAX_SCREEN_WIDTH)
				width = MAX_SCREEN_WIDTH;
			int height = (int)(rect.Height() / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
			if ((m_ImageHeight2 - startY) < height)
				height = m_ImageHeight2 - startY;
			else if (height > MAX_SCREEN_HEIGHT)
				height = MAX_SCREEN_HEIGHT;
			m_Image2.BitBlt(m_dcMem, 0, 0, width, height, startX, startY, SRCCOPY);
			dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &m_dcMem, 0, 0, SRCCOPY);
		}
	}
}

// this is called on WM_HSCROLL
void ImageDisplayerWnd::OnHScroll(UINT code, UINT pos, CScrollBar* sb)
{
	// number of pixels to be scrolled
	int delta;
	// which sb msg was sent. Set delta accordingly
	switch (code)
	{
	case SB_LINELEFT:
		delta = -LINESIZE;
		break;
	case SB_PAGELEFT:
		delta = -HPageSize_;
		break;
	case SB_THUMBTRACK:
		delta = static_cast<int>(pos)-HScrollPos_;
		break;
	case SB_PAGERIGHT:
		delta = HPageSize_;
		break;
	case SB_LINERIGHT:
		delta = LINESIZE;
		break;
	default:
		// ignore other messages
		return;
	}
	int scrollpos = HScrollPos_ + delta;
	int maxpos = ViewWidth_ - HPageSize_;
	if (scrollpos < 0)
		delta = -HScrollPos_;
	else if (scrollpos >= maxpos)
		delta = maxpos - HScrollPos_;
	// if we need to scroll do the scrolling
	if (delta != 0)
	{
		HScrollPos_ += delta;
		SetScrollPos(SB_HORZ, HScrollPos_, TRUE);
		ScrollWindow(-delta, 0);
	}
}
// This is called on WM_VSCROLL
void ImageDisplayerWnd::OnVScroll(UINT code, UINT pos, CScrollBar* sb)
{
	// number of pixels to be scrolled
	int delta;
	// which sb msg was it. set delta accordingly
	switch (code)
	{
	case SB_LINEUP:
		delta = -LINESIZE;
		break;
	case SB_PAGEUP:
		delta = -VPageSize_;
		break;
	case SB_THUMBTRACK:
		delta = static_cast<int>(pos)-VScrollPos_;
		break;
	case SB_PAGEDOWN:
		delta = VPageSize_;
		break;
	case SB_LINEDOWN:
		delta = LINESIZE;
		break;
	default:
		return;
	}
	int scrollpos = VScrollPos_ + delta;
	int maxpos = ViewHeight_ - VPageSize_;
	if (scrollpos < 0)
		delta = -VScrollPos_;
	else if (scrollpos >= maxpos)
		delta = maxpos - VScrollPos_;
	// if we need to scroll then do the scrolling
	if (delta != 0)
	{
		VScrollPos_ += delta;
		SetScrollPos(SB_VERT, VScrollPos_, TRUE);
		ScrollWindow(0, -delta);
	}
}
// this is called on WM_MOUSEWHEEL
BOOL ImageDisplayerWnd::OnMouseWheel(UINT flags, short zdelta, CPoint point)
{
	CRect rect(0, 0, 0, 0);
	GetClientRect(&rect);
	double currentXCenterPos = 0.0;
	double currentYCenterPos = 0.0;
	if (m_DisplayMap1)
	{
		currentXCenterPos = HScrollPos_ * m_ZoomFactor;
		currentYCenterPos = VScrollPos_ * m_ZoomFactor;
	}
	else
	{
		currentXCenterPos = HScrollPos_ * (m_ZoomFactor - MAX_ZOOM_FACTOR1);
		currentYCenterPos = VScrollPos_ * (m_ZoomFactor - MAX_ZOOM_FACTOR1);
	}

	m_ZoomFactor += ((double) zdelta / 300.0);
	if (m_ZoomFactor < MIN_ZOOM_FACTOR1)
		m_ZoomFactor = MIN_ZOOM_FACTOR1;
	else if ((m_ZoomFactor > MAX_ZOOM_FACTOR1) && (m_ZoomFactor < MIN_ZOOM_FACTOR2))
		m_ZoomFactor = MIN_ZOOM_FACTOR2;
	else if (m_ZoomFactor > MAX_ZOOM_FACTOR2)
		m_ZoomFactor = MAX_ZOOM_FACTOR2;
	SCROLLINFO si;
	if (m_ZoomFactor <= MAX_ZOOM_FACTOR1)
	{
		m_DisplayMap1 = true;
		ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
		ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
		HPageSize_ = rect.Width();
		VPageSize_ = rect.Height();
		HScrollPos_ = (int)(currentXCenterPos / m_ZoomFactor);
		VScrollPos_ = (int)(currentYCenterPos / m_ZoomFactor);
	}
	else
	{
		m_DisplayMap1 = false;
		ViewWidth_ = (int)(m_ImageWidth2 * (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		ViewHeight_ = (int)(m_ImageHeight2 * (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		HPageSize_ = rect.Width();
		VPageSize_ = rect.Height();
		HScrollPos_ = (int)(currentXCenterPos / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		VScrollPos_ = (int)(currentYCenterPos / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
	}
	
	si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewWidth_;
	si.nPos = HScrollPos_;
	si.nPage = HPageSize_;
	SetScrollInfo(SB_HORZ, &si, TRUE);
	si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewHeight_;
	si.nPos = VScrollPos_;
	si.nPage = VPageSize_;
	SetScrollInfo(SB_VERT, &si, TRUE);
	Invalidate();
	return TRUE;
}

bool ImageDisplayerWnd::LoadTIFFImages(CString filename)
{
	bool ret = false;
	CString message;
	WCHAR *ch1 = _T("\\");
	int index = filename.ReverseFind(*ch1);
	if (index == -1)
		return ret;
	CString filename1 = filename.Mid(index+1);

	filename1 = filename1 + _T(" - LeicaRawImageDisplayer");
	SetWindowText(filename1);
	ret = LoadScanFile(filename);
	if (ret)
		ret = CopyToRGBImage(filename);
	return ret;
}

bool ImageDisplayerWnd::CopyToRGBImage(CString filename)
{
	bool ret = false;
	m_ImageWidth2 = (m_NumColumns - 1) * NON_OVERLAP_WIDTH + TIFF_FRAME_WIDTH;
	m_ImageHeight2 = (m_NumRows - 1) * NON_OVERLAP_HEIGHT + TIFF_FRAME_HEIGHT;
	if (m_Image2 != NULL)
		m_Image2.Destroy();
	m_Image2.Create(m_ImageWidth2, -m_ImageHeight2, 24);
	int SCALE1 = (int)MAX_ZOOM_FACTOR1;
	m_ImageWidth1 = m_ImageWidth2 / SCALE1;
	m_ImageHeight1 = m_ImageHeight2 / SCALE1;
	if (m_Image1 != NULL)
		m_Image1.Destroy();
	m_Image1.Create(m_ImageWidth1, -m_ImageHeight1, 24);

	BYTE *ptr2 = (BYTE *)m_Image2.GetBits();
	BYTE *ptr1 = (BYTE *)m_Image1.GetBits();
	int pitch2 = m_Image2.GetPitch();
	int pitch1 = m_Image1.GetPitch();
	memset(ptr2, 0, sizeof(BYTE) * pitch2 * m_ImageHeight2);
	memset(ptr1, 0, sizeof(BYTE) * pitch1 * m_ImageHeight1);
	CTIFFFrame *red = new CTIFFFrame();
	CTIFFFrame *green = new CTIFFFrame();
	CTIFFFrame *blue = new CTIFFFrame();
	filename = filename.Mid(0, filename.Find(_T(".scan")));
	for (int k = 0; k < m_NumFrames; k++)
	{
		CString filename1;
		filename1.Format(_T("%s_w1_s%d_t1.TIF"), filename, k+1);
		BOOL status = red->LoadRawTIFFFile(filename1);
		CString msg;
		if (!status)
		{
			msg.Format(_T("Failed to load Red TIFF Frame %s"), filename1);
			AfxMessageBox(msg);
			break;
		}
		if ((red->m_Width != TIFF_FRAME_WIDTH) || (red->m_Height != TIFF_FRAME_HEIGHT))
		{
			msg.Format(_T("TIFF Frame Width(=%d) != %d, or TIFF Frame Height(=%d) != %d"), red->m_Width, TIFF_FRAME_WIDTH,
				red->m_Height, TIFF_FRAME_HEIGHT);
			AfxMessageBox(msg);
			break;
		}
		filename1.Format(_T("%s_w2_s%d_t1.TIF"), filename, k + 1);
		status = green->LoadRawTIFFFile(filename1);
		if (!status)
		{
			msg.Format(_T("Failed to load Green TIFF Frame %s"), filename1);
			AfxMessageBox(msg);
			break;
		}
		filename1.Format(_T("%s_w3_s%d_t1.TIF"), filename, k + 1);
		status = blue->LoadRawTIFFFile(filename1);
		if (!status)
		{
			msg.Format(_T("Failed to load Blue TIFF Frame %s"), filename1);
			AfxMessageBox(msg);
			break;
		}
		int redCutoff = m_RedCPI + m_RedCutoff;
		int greenCutoff = m_GreenCPI + m_GreenCutoff;
		int blueCutoff = m_BlueCPI + m_BlueCutoff;
		int redContrast = m_RedCPI + m_RedContrast;
		int greenContrast = m_GreenCPI + m_GreenContrast;
		int blueContrast = m_BlueCPI + m_BlueContrast;
		for (int i = 0; i < red->m_Height; i++)
		{
			if (((k % m_NumRows) < (m_NumRows-1)) && (i >= NON_OVERLAP_HEIGHT))
				continue;
			for (int j = 0; j < red->m_Width; j++)
			{
				if (((k / m_NumRows) < (m_NumColumns - 1)) && (j >= NON_OVERLAP_WIDTH))
					continue;
				BYTE redByte = GetContrastEnhancedByte(red->m_Image[red->m_Width * i + j], redContrast, redCutoff);
				BYTE greenByte = GetContrastEnhancedByte(green->m_Image[red->m_Width * i + j], greenContrast, greenCutoff);
				BYTE blueByte = GetContrastEnhancedByte(blue->m_Image[red->m_Width * i + j], blueContrast, blueCutoff);
				int ii = NON_OVERLAP_HEIGHT * (k % m_NumRows) + i;
				if (ii >= m_ImageHeight2)
				{
					continue;
				}
				int jj = (k / m_NumRows) * NON_OVERLAP_WIDTH + j;
				if (jj >= m_ImageWidth2)
				{
					continue;
				}
				ptr2[pitch2 * ii + 3 * jj] = blueByte;
				ptr2[pitch2 * ii + 3 * jj + 1] = greenByte;
				ptr2[pitch2 * ii + 3 * jj + 2] = redByte;
			}
		}
		ret = true;
	}

	if (ret)
	{
		for (int i = 0; i < m_ImageHeight1; i++)
		{
			for (int j = 0; j < m_ImageWidth1; j++)
			{
				ptr1[pitch1 * i + 3 * j] = ptr2[pitch2 * SCALE1 * i + 3 * SCALE1 * j];
				ptr1[pitch1 * i + 3 * j + 1] = ptr2[pitch2 * SCALE1 * i + 3 * SCALE1 * j + 1];
				ptr1[pitch1 * i + 3 * j + 2] = ptr2[pitch2 * SCALE1 * i + 3 * SCALE1 * j + 2];
			}
		}
	}

	delete red;
	delete green;
	delete blue;

	return ret;
}

BYTE ImageDisplayerWnd::GetContrastEnhancedByte(unsigned short value, int contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

BOOL ImageDisplayerWnd::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		return TRUE; // this doesn't need processing anymore
	}
	else if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_ESCAPE))
	{
		CRect rect(0, 0, 0, 0);
		GetClientRect(&rect);
		
		m_ZoomFactor = MIN_ZOOM_FACTOR1;
		ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
		ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
		m_DisplayMap1 = true;
		ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
		ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
		HPageSize_ = rect.Width();
		VPageSize_ = rect.Height();
		HScrollPos_ = 0;
		VScrollPos_ = 0;
		SCROLLINFO si;
		si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
		si.nMin = 0;
		si.nMax = ViewWidth_;
		si.nPos = HScrollPos_;
		si.nPage = HPageSize_;
		SetScrollInfo(SB_HORZ, &si, TRUE);
		si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
		si.nMin = 0;
		si.nMax = ViewHeight_;
		si.nPos = VScrollPos_;
		si.nPage = VPageSize_;
		SetScrollInfo(SB_VERT, &si, TRUE);
		Invalidate();
		return TRUE;
	}
	return CFrameWnd::PreTranslateMessage(pMsg);
}

bool ImageDisplayerWnd::LoadScanFile(CString filename)
{
	bool ret = false;
	CStdioFile theFile;
	if (theFile.Open(filename, CFile::modeRead | CFile::typeText))
	{
		CString textline;
		CString previousLine;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Overview")) > -1)
			{
				CString subStr = _T("Stage");
				int index = textline.Find(subStr);
				textline = textline.Mid(index + subStr.GetLength());
				index = textline.Find(_T(","));
				textline = textline.Mid(0, index - 1);
				m_NumFrames = _wtoi(textline) - 1;
				subStr = _T("Row");
				index = previousLine.Find(subStr);
				previousLine = previousLine.Mid(index + subStr.GetLength());
				index = previousLine.Find(_T("_"));
				textline = previousLine.Mid(0, index);
				m_NumRows = _wtoi(textline) + 1;
				subStr = _T("Col");
				index = previousLine.Find(subStr);
				previousLine = previousLine.Mid(index + subStr.GetLength());
				index = previousLine.Find(_T("\""));
				textline = previousLine.Mid(0, index);
				m_NumColumns = _wtoi(textline) + 1;
				ret = true;
				break;
			}
			else
				previousLine = textline;
		}
		theFile.Close();
	}
	return ret;
}

bool ImageDisplayerWnd::SaveOverlayImageToFile(CString pathname, CString safeFilename)
{
	bool ret = false;

	if (SaveBMPImage(pathname + _T("\\") + safeFilename, &m_Image2))
		ret = true;

	return ret;
}

BOOL ImageDisplayerWnd::SaveBMPImage(CString filename, CImage *image)
{
	BOOL ret = FALSE;

	CFile fileToSave;
	ret = fileToSave.Open(filename, CFile::modeCreate | CFile::modeWrite);
	if (ret)
	{
		BITMAPINFOHEADER BMIH;
		memset(&BMIH, 0, 40);
		BMIH.biSize = 40;
		BMIH.biSizeImage = image->GetPitch() * image->GetHeight();
		BMIH.biWidth = image->GetWidth();
		BMIH.biHeight = image->GetHeight();
		if (BMIH.biHeight > 0)
			BMIH.biHeight = -BMIH.biHeight;
		BMIH.biPlanes = 1;
		BMIH.biBitCount = 24;
		BMIH.biCompression = BI_RGB;

		BITMAPFILEHEADER bmfh;
		memset(&bmfh, 0, 14);
		int nBitsOffset = 14 + BMIH.biSize;
		LONG lImageSize = BMIH.biSizeImage;
		LONG lFileSize = nBitsOffset + lImageSize;

		bmfh.bfType = 'B' + ('M' << 8);
		bmfh.bfOffBits = nBitsOffset;
		bmfh.bfSize = lFileSize;
		bmfh.bfReserved1 = bmfh.bfReserved2 = 0;

		fileToSave.Write(&bmfh, 14);
		fileToSave.Write(&BMIH, 40);
		fileToSave.Write(image->GetBits(), lImageSize);
		fileToSave.Close();
	}

	return ret;
}